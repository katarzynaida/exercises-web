# Excercises in fronted-end development

This repository was created to keep track of my progress in front-end development.

So far I created following pages:

1. [Simple page with table](https://kasianastazu.yum.pl/tabela/)
1. [Przepisy](https://kasianastazu.yum.pl/przepisy/)
1. [Business card](https://kasianastazu.yum.pl/wizytowka/)